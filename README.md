# Partie II

Lien vers l’exercice : https://codepen.io/vlaude/pen/rQNGoz?editors=0010

Dans cette deuxième partie nous allons :
- Gérer les **interactions utilisateur** (clique de souris)
- Mettre à jour **l’état** de l’application en fonction des cliques utilisateur
- Observer le **partage automatique** du nouvel état de l’application entre les components

L’objectif est d’afficher les cartes du plateau (qui sont désormais cachées par défaut) en cliquant dessus.

**Résultat attendu :**

![](images/Resultat.PNG)


Dans cette seconde partie, vous serez volontairement moins guidé que dans la première, voici cependant quelques éléments qui sont susceptible de vous aider :
- une nouvelle propriété de type booléen est ajoutée aux cartes et permet de savoir si on doit afficher sa valeur ou non

![](images/isDisplay.PNG)

- Des fonctions javascript peuvent être injecté entre component de la même manière que des variables grâce à l’objet **props**

![](images/click.png)

![](images/onClick2.PNG)


- N’oubliez pas que c’est le component Application qui gère **l’état** de l’application. Les cliques sur les cartes devront donc remonter jusqu’à lui et c’est lui qui fera le traitement adéquat.

























